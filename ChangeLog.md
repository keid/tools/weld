# Changelog for keid-weld

## 0.1.2.0 -- WIP

- Add `Resource.Gltf.Weld.Settings`.

## 0.1.1.0

- Changed mesh codec to use `Serialise` class instead of `Storable`.
