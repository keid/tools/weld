module Resource.Gltf.Weld.Settings
  ( Settings(..)
  , load
  , storeFile

  , parseTransform

  , gltfMiddle
  , gltfMidBottom
  ) where

import RIO

import Data.Yaml ((.:?), (.!=), (.=))
import Data.Yaml qualified as Yaml
import Data.Yaml.Pretty qualified as YamlPretty
import Geomancy (Transform, Vec3, vec3, withVec3)
import Geomancy.Transform qualified as Transform
import Geomancy.Vec3 qualified as Vec3
import GHC.Stack (withFrozenCallStack)
import RIO.ByteString qualified as ByteString
import RIO.Directory (createDirectoryIfMissing)
import RIO.FilePath (takeDirectory)
import RIO.Text qualified as Text

import Resource.Gltf.Weld qualified as Weld
import Resource.Source (Source)
import Resource.Source qualified as Source

data Settings = Settings
  { transformStack :: [Text]
  , originX        :: Weld.Origin
  , originY        :: Weld.Origin
  , originZ        :: Weld.Origin
  , adjustOrigin   :: Vec3
  , doubleSide     :: Bool -- ^ Append reverse indices to meshes with double-sided material.
  , reverseIndices :: Bool -- ^ Reverse mesh indices (to clockwise order) without changing vertices.
  , materialOffset :: Int  -- ^ Add offset for each material ID used.
  , textureOffset  :: Int  -- ^ Add offset for each texture ID used.
  }
  deriving (Show, Generic)

instance Yaml.FromJSON Settings where
  parseJSON = Yaml.withObject "Settings" \o -> do
    transformStack <- o .:? "transform-stack" .!= mempty

    originX <- (o .:? "origin-x" .!= "keep") >>= \case
      "left"   -> pure Weld.OriginXLeft
      "middle" -> pure Weld.OriginXMiddle
      "mean"   -> pure Weld.OriginXMean
      "right"  -> pure Weld.OriginXRight
      "keep"   -> pure Weld.OriginKeep
      ox       -> fail $ "Unexpected origin-x" <> show @Text ox

    originY <- (o .:? "origin-y" .!= "keep") >>= \case
      "top"    -> pure Weld.OriginYTop
      "middle" -> pure Weld.OriginYMiddle
      "mean"   -> pure Weld.OriginYMean
      "bottom" -> pure Weld.OriginYBottom
      "keep"   -> pure Weld.OriginKeep
      oy       -> fail $ "Unexpected origin-x" <> show @Text oy

    originZ <- (o .:? "origin-z" .!= "keep") >>= \case
      "near"   -> pure Weld.OriginZNear
      "middle" -> pure Weld.OriginZMiddle
      "mean"   -> pure Weld.OriginZMean
      "far"    -> pure Weld.OriginZFar
      "keep"   -> pure Weld.OriginKeep
      oz       -> fail $ "Unexpected origin-x" <> show @Text oz

    adjustOrigin <- fmap Vec3.fromTuple (o .:? "origin" .!= (0, 0, 0))

    doubleSide <- o .:? "double-side" .!= False
    reverseIndices <- o .:? "reverse" .!= False
    materialOffset <- o .:? "material-offset" .!= 0
    textureOffset <- o .:? "texture-offset" .!= 0

    pure Settings{..}

load
  :: ( HasCallStack
     , MonadIO io
     , MonadThrow io
     , MonadReader env io
     , HasLogFunc env
     )
  => Source -> io Settings
load = withFrozenCallStack $ Source.load Yaml.decodeThrow

storeFile :: MonadIO io => Settings -> FilePath -> io ()
storeFile settings path = do
  createDirectoryIfMissing True (takeDirectory path)
  ByteString.writeFile path $
    YamlPretty.encodePretty
      (YamlPretty.defConfig
        & YamlPretty.setConfDropNull True
        & YamlPretty.setConfCompare compare
      )
      settings

instance Yaml.ToJSON Settings where
  toJSON Settings{..} = Yaml.object
    [ "transform-stack" .= transformStack

    , "origin-x" .= case originX of
        Weld.OriginXLeft   -> Yaml.String "left"
        Weld.OriginXMiddle -> Yaml.String "middle"
        Weld.OriginXMean   -> Yaml.String "mean"
        Weld.OriginXRight  -> Yaml.String "right"
        Weld.OriginKeep    -> Yaml.Null
        _                  -> error "please don't"

    , "origin-y" .= case originY of
        Weld.OriginYTop    -> Yaml.String "top"
        Weld.OriginYMiddle -> Yaml.String "middle"
        Weld.OriginYMean   -> Yaml.String "mean"
        Weld.OriginYBottom -> Yaml.String "bottom"
        Weld.OriginKeep    -> Yaml.Null
        _                  -> error "please don't"

    , "origin-z" .= case originZ of
        Weld.OriginZNear   -> Yaml.String "near"
        Weld.OriginZMiddle -> Yaml.String "middle"
        Weld.OriginZMean   -> Yaml.String "mean"
        Weld.OriginZFar    -> Yaml.String "far"
        Weld.OriginKeep    -> Yaml.Null
        _                  -> error "please don't"

    , "origin" .= withVec3 adjustOrigin \x y z ->
        nullify (0, 0, 0) (x, y, z)

    , "double-side"     .= nullify False doubleSide
    , "reverse-indices" .= nullify False reverseIndices
    , "material-offset" .= nullify 0 materialOffset
    , "texture-offset"  .= nullify 0 textureOffset
    ]
    where
      nullify :: (Eq a, Yaml.ToJSON a) => a -> a -> Yaml.Value
      nullify theNull value =
        if value == theNull then
          Yaml.Null
        else
          Yaml.toJSON value

parseTransform :: Text -> Maybe Transform
parseTransform arg =
  case break (':' ==) (Text.unpack arg) of
    ("turn-x", ':' : turns) -> fmap (Transform.rotateX . turns2rad) $ readMaybe turns
    ("turn-y", ':' : turns) -> fmap (Transform.rotateY . turns2rad) $ readMaybe turns
    ("turn-z", ':' : turns) -> fmap (Transform.rotateZ . turns2rad) $ readMaybe turns

    ("flip", ':' : "x") -> Just $ Transform.scale3 (-1) 1 1
    ("flip", ':' : "y") -> Just $ Transform.scale3 1 (-1) 1
    ("flip", ':' : "z") -> Just $ Transform.scale3 1 1 (-1)

    ("scale",   ':' : num)  -> Transform.scale <$> readMaybe num

    -- ("translate", ':' : )
    -- ("axis-angle", ':' : axisAngle)

    _ -> Nothing

  where
    turns2rad = (* τ)

gltfMiddle :: Settings
gltfMiddle = Settings
  { transformStack = ["turn-z:0.5"]
  , originX        = Weld.OriginXMiddle
  , originY        = Weld.OriginYMiddle
  , originZ        = Weld.OriginZMiddle
  , adjustOrigin   = 0
  , doubleSide     = False
  , reverseIndices = False
  , materialOffset = 0
  , textureOffset  = 0
  }

gltfMidBottom :: Settings
gltfMidBottom = gltfMiddle
  { originX      = Weld.OriginXMiddle
  , originY      = Weld.OriginYBottom
  , originZ      = Weld.OriginZMiddle
  , adjustOrigin = vec3 0 0.5 0
  }

τ :: Float
τ = 2 * pi
