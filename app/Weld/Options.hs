module Weld.Options
  ( Options(..)
  , getOptions
  , optionsP

  , Command(..)
  ) where

import RIO

import Options.Applicative.Simple qualified as Opt

import Paths_keid_weld qualified
import Weld.Command.Gltf qualified as Gltf
import Weld.Command.TileMap qualified as TileMap

-- | Command line arguments
data Options = Options
  { optionsVerbose :: Bool
  , optionsCommand :: Command
  , optionsWorkdir :: Maybe FilePath
  }
  deriving (Show)

data Command
  = CommandGltf Gltf.Options
  | CommandTileMap TileMap.Options
  -- TODO: CommandFont Font.Options
  -- TODO: CommandTexture Texture.Options
  deriving (Show)

getOptions :: IO Options
getOptions = do
  (options, ()) <- Opt.simpleOptions
    $(Opt.simpleVersion Paths_keid_weld.version)
    header
    description
    optionsP
    Opt.empty
  pure options
  where
    header =
      "Playground welding tool"

    description =
      mempty

optionsP :: Opt.Parser Options
optionsP = do
  optionsVerbose <- Opt.switch $ mconcat
    [ Opt.long "verbose"
    , Opt.short 'v'
    , Opt.help "Show more and more detailed messages"
    ]

  optionsWorkdir <- Opt.optional . Opt.strOption $ mconcat
    [ Opt.long "work-dir"
    , Opt.help "Change running directory"
    , Opt.metavar "PATH"
    ]

  optionsCommand <- Opt.hsubparser $ mconcat
    [ gltf
    , tilemap
    -- , TODO: font
    -- , TODO: texture
    ]

  pure Options{..}

-- * Commands

-- ** GLTF

gltf :: Opt.Mod Opt.CommandFields Command
gltf = Opt.command "gltf" . Opt.info gltfP $ Opt.progDesc "Compress glTF scene"

gltfP :: Opt.Parser Command
gltfP = fmap CommandGltf do
  optionsInput <- Opt.strOption $ mconcat
    [ Opt.long "input"
    , Opt.short 'i'
    , Opt.help "Input glTF scene file."
    , Opt.metavar "resources.in/SOMETHING/scene.gltf"
    ]

  optionsOutput <- Opt.strOption $ mconcat
    [ Opt.long "output"
    , Opt.short 'o'
    , Opt.help "Output mesh file."
    , Opt.metavar "resources/meshes/SOMETHING.mesh"
    ]

  optionsModel <- Opt.option modelPipeline $ mconcat
    [ Opt.long "model-pipeline"
    , Opt.short 'p'
    , Opt.help "Model pipeline that will be used to render a mesh."
    ]

  optionsValidate <- Opt.switch $ mconcat
    [ Opt.long "validate"
    , Opt.help "Load output data back and compare with input."
    ]

  optionsMeasure <- Opt.switch $ mconcat
    [ Opt.long "measure"
    , Opt.help "Only measure the resulting mesh, but don't write the output file."
    ]

  pure Gltf.Options{..}

modelPipeline :: Opt.ReadM Gltf.ModelPipeline
modelPipeline = Opt.maybeReader \case
  "lit:colored"    -> Just Gltf.ModelColoredLit
  "lit:material"   -> Just Gltf.ModelMaterialLit
  "lit:textured"   -> Just Gltf.ModelTexturedLit
  "unlit:colored"  -> Just Gltf.ModelColoredUnlit
  "unlit:textured" -> Just Gltf.ModelTexturedUnlit
  _                -> Nothing

-- ** TileMap

tilemap :: Opt.Mod Opt.CommandFields Command
tilemap = Opt.command "tilemap" . Opt.info tilemapP $ Opt.progDesc "Encode tilemap images"

tilemapP :: Opt.Parser Command
tilemapP = fmap CommandTileMap do
  optionsInput <- Opt.strOption $ mconcat
    [ Opt.long "input"
    , Opt.short 'i'
    , Opt.help "A layer image file to encode"
    , Opt.metavar "resources.in/tilesets/example/LAYER-0.png"
    ]

  optionsOutput <- Opt.strOption $ mconcat
    [ Opt.long "output"
    , Opt.short 'o'
    , Opt.help "Resulting texture file"
    , Opt.metavar "resources/tilesets/example/LAYER-0.ktx.zst"
    ]

  optionsFormat <- Opt.option readFormat $ mconcat
    [ Opt.long "format"
    , Opt.short 'f'
    , Opt.help "Tile format"
    , Opt.metavar "xy8 | xy8-tiled"
    , Opt.value TileMap.XY8
    ]

  optionsTilesetWidth <- Opt.optional . Opt.option Opt.auto $ mconcat
    [ Opt.long "tileset-width"
    , Opt.short 'W'
    , Opt.metavar "TILES"
    ]

  optionsTilesetHeight <- Opt.optional . Opt.option Opt.auto $ mconcat
    [ Opt.long "tileset-width"
    , Opt.short 'H'
    , Opt.metavar "TILES"
    ]

  optionsTilesetImage <- Opt.optional . Opt.strOption $ mconcat
    [ Opt.long "tileset-image"
    , Opt.short 'I'
    , Opt.metavar "FILE"
    ]

  pure TileMap.Options{..}

readFormat :: Opt.ReadM TileMap.Format
readFormat =
  Opt.str >>= \case
    "xy8" ->
      pure TileMap.XY8
    "xy8-tiled" ->
      pure TileMap.XY8Tiled
    huh ->
      fail $ "Unexpected tile format: " <> huh
