{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE OverloadedLists #-}

module Weld.Command.TileMap
  ( Options(..)
  , Format(..)
  , run
  ) where

import RIO

import Codec.Ktx qualified as Ktx1
import Codec.Ktx.KeyValue qualified as KtxKV
import Codec.Ktx2.Header qualified as Ktx2
import Codec.Ktx2.Write qualified as Ktx2Write
import Codec.Ktx2.DFD qualified as DFD
import Codec.Ktx2.DFD.Khronos.BasicV2 qualified as DFD
import Codec.Picture qualified as Picture
import Codec.Tiled.Layer qualified as Layer
import Codec.Tiled.Layer.Data qualified as LayerData
import Codec.Tiled.Map qualified as TiledMap
import Codec.Tiled.Map.IO qualified as TiledMap
import Codec.Tiled.Tileset qualified as Tileset
import Codec.Tiled.Tileset.IO qualified as Tileset (readFile)
import Codec.Tiled.Tileset.Ref qualified as Tiled
import Data.ByteString.Base64 (decodeBase64)
import Data.ByteString.Builder qualified as Builder
import Data.ByteString.Internal qualified as BSI
import Data.ByteString.Lazy qualified as BSL
import Data.Csv qualified as CSV
import Data.Tiled.GID (GID(..))
import Data.Tiled.GID qualified as Tiled
import Data.Type.Equality (type (~))
import Data.Vector.Storable qualified as Storable (force, unsafeCast)
import Foreign qualified
import Resource.Compressed.Zstd qualified  as Zstd
import RIO.ByteString qualified as BS
import RIO.Directory (doesDirectoryExist)
import RIO.FilePath (takeBaseName, splitFileName, splitExtensions, (</>), (<.>))
import RIO.FilePath qualified as FilePath
import RIO.Map qualified as Map
import RIO.Vector qualified as Vector
import RIO.Vector.Partial qualified as Vector (head)
import RIO.Vector.Storable.Unsafe qualified as Storable
import Vulkan.Core10.Enums.Format qualified as Vk

data Options = Options
  { optionsInput         :: FilePath
  , optionsOutput        :: FilePath -- file or directory
  , optionsFormat :: Format

  , optionsTilesetWidth  :: Maybe Word32  -- for CSV, Tiled
  , optionsTilesetHeight :: Maybe Word32  -- for Tiled
  , optionsTilesetImage  :: Maybe FilePath -- for Tiled, relative to input (source)
  }
  deriving (Show)

data Format
  = XY8
  | XY8Tiled
  deriving (Eq, Ord, Show, Enum, Bounded)

run :: HasLogFunc env => Options -> RIO env ()
run Options{..} = do

  let
    (inputDir, inputName) = splitFileName optionsInput
    (inputBaseName, _inputExtension) = splitExtensions inputName

  outputName <-
    doesDirectoryExist optionsOutput >>= \case
      True ->
        pure $ optionsOutput </> inputBaseName <.> "ktx.zst"
      False ->
        pure optionsOutput

  when (outputName == inputName) do
    logError "Output name matches input, aborting."
    exitFailure

  optionsTilesetImage' <- for optionsTilesetImage \tilesetImageName ->
    case splitFileName tilesetImageName of
      ("./", relative) ->
        pure $ inputDir </> relative
      _otherwise ->
        pure tilesetImageName

  case optionsFormat of
    XY8 -> do
      tilemap <- readMapFile
        @TileXY8
        @Picture.PixelYA8
        optionsTilesetWidth
        optionsInput

      encode
        @TileXY8
        @Picture.PixelYA8
        optionsTilesetWidth
        optionsTilesetHeight
        optionsTilesetImage'
        outputName
        tilemap

    XY8Tiled -> do
      tilemap <- readMapFile
        @TileXY8Flags
        @Picture.PixelRGBA8
        optionsTilesetWidth
        optionsInput

      encode
        @TileXY8Flags
        @Picture.PixelRGBA8
        optionsTilesetWidth
        optionsTilesetHeight
        optionsTilesetImage'
        outputName
        tilemap

readMapFile
  :: forall tile pixel env
  . ( FromGlobalId tile
    , Storable tile
    , FromPixel pixel
    , Picture.Pixel pixel
    , HasLogFunc env
    )
  => Maybe Word32
  -> FilePath
  -> RIO env (Picture.Image pixel)
readMapFile tilesetWidthOpt input = do
  let (_inputBaseName, inputExtension) = splitExtensions input
  logDebug $ displayShow (input, inputExtension)
  case inputExtension of
    ".csv" ->
      decodeCSV @tile @pixel tilesetWidthOpt input

    ".ktx" ->
      error "TODO: decodeKtx"

    ".ktx.zst" ->
      error "TODO: decodeKtxZst"

    ".tmj" ->
      decodeTiled @tile @pixel input

    _imageExt -> do
      if lossyFormat inputExtension then do
        logError "You really should not store tilemaps in lossy formats."
        exitFailure
      else
        decodeImage @pixel input

lossyFormat :: FilePath -> Bool
lossyFormat ext = elem @[] ext
  [ ".jpg"
  ]

decodeCSV
  :: forall tile pixel env
  . ( Storable (Picture.PixelBaseComponent pixel)
    , Storable tile
    , FromGlobalId tile
    , HasLogFunc env
    )
  => Maybe Word32
  -> FilePath
  -> RIO env (Picture.Image pixel)
decodeCSV mTilesetWidth inputName = do
  tilesetWidth <- csvTilesetWidth mTilesetWidth

  contents <- liftIO $ BSL.readFile inputName
  case CSV.decode CSV.NoHeader contents of
    Left err -> do
      logError $ fromString err
      exitFailure
    Right idsTable -> do
      let
        imageHeight = Vector.length idsTable

        rowLengths =
          nubOrd . toList $
            Vector.map Vector.length idsTable

      imageWidth <- case rowLengths of
        [width] ->
          pure width
        [] -> do
          logError "Empty map"
          exitFailure
        confusing -> do
          logError $ "Map has different column count: " <> displayShow confusing
          exitFailure

      let
        tilesTable =
          Vector.map
            (Vector.map $ fromGlobalId @tile FIRST_GID tilesetWidth . GID)
            idsTable

        imageData =
          Storable.unsafeCast . Vector.fromList $
            concatMap (Vector.toList @Vector) tilesTable

      pure Picture.Image{..}

decodeTiled
  :: forall tile pixel env
  . ( Storable (Picture.PixelBaseComponent pixel)
    , Storable tile
    , FromGlobalId tile
    , HasLogFunc env
    )
  => FilePath
  -> RIO env (Picture.Image pixel)
decodeTiled inputName = do
  tm <- TiledMap.readFile inputName

  tilesetRef0 <- pickOne "tileset" $ TiledMap.tilesets tm
  tileset0 <- case tilesetRef0 of
      Tiled.TilesetEmbedded{firstGid=_, embedded} ->
        pure embedded
      Tiled.TilesetRef{firstGid=_, source} -> do
        let tilesetPath = FilePath.takeDirectory inputName </> source
        logDebug $ "Loading external tileset from " <> displayShow tilesetPath
        Tileset.readFile tilesetPath

  layer0 <- pickOne "layer" $ TiledMap.layers tm

  let
    tilesetWidth = fromIntegral $ Tileset.columns tileset0

    layerWidth = fromMaybe 1 (Layer.width layer0)
    layerHeight = fromMaybe 1 (Layer.height layer0)

  layerData <-
    case Layer.data_ layer0 of
      Just (LayerData.GIDs gids) ->
        pure gids
      Just (LayerData.Base64 text) -> do
        decoded <- case decodeBase64 (encodeUtf8 text) of
          Right ok ->
            pure ok
          Left err -> do
            logError $ "Error decoding base64 layer data: " <> display err
            exitFailure

        for_ (Layer.compression layer0) \compression ->
          logDebug $ "Layer data is compressed with " <> displayShow compression
        decompressed <- case Layer.compression layer0 of
          Nothing ->
            pure decoded
          Just "zlib" ->
            error "TODO: LayerData.Base64 + zlib"
          Just "gzip" ->
            error "TODO: LayerData.Base64 + gzip"
          Just "zstd" ->
            case Zstd.decompressBytes (Zstd.Compressed decoded) of
              Right ok ->
                pure ok
              Left (Zstd.ZstdError err) -> do
                logError $ "Error decompressing zstd layer data: " <> display err
                exitFailure
              Left (Zstd.EmptyFile _huh) -> do
                logError "Error decompressing zstd layer data (empty data)"
                exitFailure
          Just huh -> do
            logError $ "Unexpected layer data compression: " <> displayShow huh
            exitFailure

        let
          (bytesPtr, offset, len) = BSI.toForeignPtr decompressed
          gidBytes = Storable.unsafeFromForeignPtr bytesPtr offset len
        pure $ Storable.force (Storable.unsafeCast gidBytes)
      Nothing ->
        mempty

  let
    tilesData =
        Vector.map
          (fromGlobalId @tile FIRST_GID tilesetWidth)
          layerData

    bytesData =
      Storable.unsafeCast @tile @(Picture.PixelBaseComponent pixel) tilesData

  pure Picture.Image
    { imageWidth  = layerWidth
    , imageHeight = layerHeight
    , imageData   = bytesData
    }

pickOne
  :: HasLogFunc env
  => Utf8Builder
  -> Vector a
  -> RIO env a
pickOne title = \case
  [] -> do
    logError $ "Map contains no " <> title
    exitFailure
  [one] ->
    pure one
  layers -> do
    let
      restLen = Vector.length layers - 1
      plural = if restLen /= 1 then "" else "s"
    logWarn $ "Ignoring " <> displayShow restLen <> " " <> title <> plural
    pure $ Vector.head layers

encodeTiled
  :: forall tile pixel env
  . ( Picture.PixelBaseComponent pixel ~ Word8
    , ToGlobalId tile
    , Storable tile
    , HasLogFunc env
    )
  => Maybe Word32
  -> Maybe Word32
  -> Maybe FilePath
  -> FilePath
  -> Picture.Image pixel
  -> RIO env ()
encodeTiled tilesetWidthOpt tilesetHeightOpt tilesetImageOpt outputName tilemap = do
  (tilesetWidth, tilesetHeight, tilesetImageName) <-
    case (,,) <$> tilesetWidthOpt <*> tilesetHeightOpt <*> tilesetImageOpt of
      Nothing -> do
        logError "Tiled encoding requires --tileset-width and --tileset-image"
        exitFailure
      Just ts ->
        pure ts
  let
    layerData = LayerData.GIDs $
      Vector.map
        (toGlobalId @tile FIRST_GID tilesetWidth)
        ( Vector.convert . Storable.unsafeCast $
            Picture.imageData tilemap
        )

  (tilesetImage, tilesetImageMeta) <- liftIO $
    Picture.readImageWithMetadata tilesetImageName >>=
      either throwString pure
  logDebug $ displayShow tilesetImageMeta

  let
    tilesetImageSize = Picture.dynamicMap (Picture.imageWidth &&& Picture.imageHeight) tilesetImage

    tilesetParams = mkTileset
      (fromIntegral tilesetWidth)
      (fromIntegral tilesetHeight)
      tilesetImageName
      tilesetImageSize

  TiledMap.writeFile outputName $
    tiled tilesetParams layerData
  where
    mkTileset tilesetWidth tilesetHeight tilesetImageName (tilesetImageWidth, tilesetImageHeight) = Tileset.empty
      { Tileset.image = tilesetImageName -- XXX: absolute
      , Tileset.name = fromString $ takeBaseName tilesetImageName
      , Tileset.tileWidth = tilesetImageWidth `div` tilesetWidth
      , Tileset.tileHeight = tilesetImageHeight `div` tilesetHeight
      , Tileset.imageWidth = tilesetImageWidth
      , Tileset.imageHeight = tilesetImageHeight
      , Tileset.columns = tilesetWidth
      , Tileset.tileCount = tilesetWidth * tilesetHeight
      }

    tiled tileset layerData = TiledMap.empty
      { TiledMap.width = Picture.imageWidth tilemap
      , TiledMap.height = Picture.imageHeight tilemap
      , TiledMap.tileWidth = Tileset.tileWidth tileset
      , TiledMap.tileHeight = Tileset.tileHeight tileset
      , TiledMap.orientation = TiledMap.ORTHOGONAL
      , TiledMap.layers = [layer layerData]
      , TiledMap.tilesets = [Tiled.TilesetEmbedded (GID 1) tileset]
      , TiledMap.nextObjectId = 1
      }

    layer layerData = Layer.empty
      { Layer.width      = Just $ Picture.imageWidth tilemap
      , Layer.height     = Just $ Picture.imageHeight tilemap
      , Layer.name       = "Tile Layer 1"
      , Layer.type_      = "tilelayer"
      , Layer.data_      = Just layerData
      }

-- TODO: make configurable
pattern FIRST_GID :: GID
pattern FIRST_GID = GID 1

data TileXY8 = TileXY8 !Word8 !Word8
  deriving (Eq, Ord, Show)

instance Storable TileXY8 where
  sizeOf ~_ = 2
  alignment ~_ = 2

  peek ptr = TileXY8
    <$> Foreign.peekByteOff ptr 0
    <*> Foreign.peekByteOff ptr 1

  poke ptr (TileXY8 x y) = do
    Foreign.pokeByteOff ptr 0 x
    Foreign.pokeByteOff ptr 1 y

data TileXY8Flags = TileXY8Flags !Word8 !Word8 Tiled.Flags
  deriving (Eq, Show)

instance Storable TileXY8Flags where
  sizeOf ~_ = 4
  alignment ~_ = 4

  peek ptr = TileXY8Flags
    <$> Foreign.peekByteOff ptr 0
    <*> Foreign.peekByteOff ptr 1
    <*> fmap Tiled.unpack8 (Foreign.peekByteOff ptr 3)

  poke ptr (TileXY8Flags x y flags) = do
    Foreign.pokeByteOff ptr 0 x
    Foreign.pokeByteOff ptr 1 y
    Foreign.pokeByteOff ptr 3 $ Tiled.pack8 flags

class ToGlobalId a where
  toGlobalId :: GID -> Word32 -> a -> GID

instance ToGlobalId TileXY8 where
  {-# INLINE toGlobalId #-}
  toGlobalId (GID firstGid) tilesetWidth = \case
    TileXY8 0xFF 0xFF ->
      GID 0xFFFFFFFF
    TileXY8 x y ->
      Tiled.fromLocal Tiled.noFlags . fromIntegral $
        firstGid +
        fromIntegral y * tilesetWidth +
        fromIntegral x

instance ToGlobalId TileXY8Flags where
  {-# INLINE toGlobalId #-}
  toGlobalId (GID firstGid) tilesetWidth = \case
    TileXY8Flags 0xFF 0xFF _flags ->
      GID 0xFFFFFFFF
    TileXY8Flags x y flags ->
      Tiled.fromLocal flags . fromIntegral $
        firstGid +
        fromIntegral y * tilesetWidth +
        fromIntegral x

instance ToGlobalId Picture.PixelRGBA8 where
  {-# INLINE toGlobalId #-}
  toGlobalId (GID firstGid) tilesetWidth = \case
    Picture.PixelRGBA8 x y _unused bits8 ->
      Tiled.fromLocal (Tiled.unpack8 bits8) . fromIntegral $
        firstGid +
        fromIntegral y * tilesetWidth +
        fromIntegral x

class FromGlobalId a where
  fromGlobalId :: GID -> Word32 -> GID -> a

instance FromGlobalId TileXY8 where
  {-# INLINE fromGlobalId #-}
  fromGlobalId (GID firstGid) tilesetWidth = \case
    GID 0xFFFFFFFF ->
      TileXY8 0xFF 0xFF
    gid ->
      TileXY8 (fromIntegral x) (fromIntegral y)
      where
        (y, x) = divMod (fromIntegral (Tiled.toLocal gid) - firstGid) tilesetWidth

instance FromGlobalId TileXY8Flags where
  {-# INLINE fromGlobalId #-}
  fromGlobalId (GID firstGid) tilesetWidth = \case
    GID 0xFFFFFFFF ->
      TileXY8Flags 0xFF 0xFF Tiled.noFlags
    gid ->
      TileXY8Flags
        (fromIntegral x)
        (fromIntegral y)
        (Tiled.flags gid)
      where
        (y, x) = divMod (fromIntegral (Tiled.toLocal gid) - firstGid) tilesetWidth

instance FromGlobalId Picture.PixelRGBA8 where
  {-# INLINE fromGlobalId #-}
  fromGlobalId (GID firstGid) tilesetWidth = \case
    GID 0xFFFFFFFF ->
      Picture.PixelRGBA8 0xFF 0xFF 0xFF 0xFF

    gid ->
      Picture.PixelRGBA8
        (fromIntegral x)
        (fromIntegral y)
        0
        (Tiled.pack8 $ Tiled.flags gid)

      where
        (y, x) = divMod (fromIntegral (Tiled.toLocal gid) - firstGid) tilesetWidth

class FromPixel a where
  fromRGB :: Picture.PixelRGB8 -> a
  fromRGBA :: Picture.PixelRGBA8 -> a

instance FromPixel Picture.PixelYA8 where
  {-# INLINE fromRGB #-}
  fromRGB (Picture.PixelRGB8 r g _b) = Picture.PixelYA8 r g

  {-# INLINE fromRGBA #-}
  fromRGBA (Picture.PixelRGBA8 r g _b _a) = Picture.PixelYA8 r g

instance FromPixel Picture.PixelRGBA8 where
  {-# INLINE fromRGB #-}
  fromRGB (Picture.PixelRGB8 r g b) = Picture.PixelRGBA8 r g b 0

  {-# INLINE fromRGBA #-}
  fromRGBA = id

class ToGlFormat a where
  toGlFormat :: (Word32, Word32, Word32, Word32)

instance ToGlFormat Picture.PixelYA8 where
  toGlFormat =
    ( GL_UNSIGNED_BYTE
    , 1
    , GL_RG
    , GL_RG8
    )

instance ToGlFormat Picture.PixelRGBA8 where
  toGlFormat =
    ( GL_UNSIGNED_BYTE
    , 1
    , GL_RGBA
    , GL_RGBA8
    )

class ToVkFormat a where
  toVkFormat :: (Vk.Format, Word32)

instance ToVkFormat Picture.PixelYA8 where
  toVkFormat = (Vk.FORMAT_R8G8_UNORM, 1)

instance ToVkFormat Picture.PixelRGBA8 where
  toVkFormat = (Vk.FORMAT_R8G8B8A8_UNORM, 1)

decodeImage
  :: ( Picture.Pixel pixel
    , FromPixel pixel
    , HasLogFunc env
    )
  => FilePath
  -> RIO env (Picture.Image pixel)
decodeImage inputName = do
  logDebug $ "Reading image from " <> fromString inputName
  (image, meta) <- liftIO $
    Picture.readImageWithMetadata inputName >>=
      either throwString pure

  logDebug $ displayShow meta

  case image of
    Picture.ImageRGB8 rgb8 ->
      pure $ Picture.pixelMap fromRGB rgb8
    Picture.ImageRGBA8 rgba8 ->
      pure $ Picture.pixelMap fromRGBA rgba8
    _ ->
      throwString "Unexpected image format."

pattern GL_UNSIGNED_BYTE :: Word32
pattern GL_UNSIGNED_BYTE = 5121

pattern GL_RG :: Word32
pattern GL_RG = 33319

pattern GL_RG8 :: Word32
pattern GL_RG8 = 33323

pattern GL_RGBA :: Word32
pattern GL_RGBA = 6408

pattern GL_RGBA8 :: Word32
pattern GL_RGBA8 = 32856

encode
  :: forall tile pixel env
  . ( Picture.PixelBaseComponent pixel ~ Word8
    , ToGlFormat pixel
    , ToVkFormat pixel
    , ToGlobalId tile
    , Storable tile
    , HasLogFunc env
    )
  => Maybe Word32
  -> Maybe Word32
  -> Maybe FilePath
  -> FilePath
  -> Picture.Image pixel
  -> RIO env ()
encode tilesetWidthOpt tilesetHeightOpt tilesetImageOpt outputName tilemap = do
  logDebug $ "Writing tilemap to " <> fromString outputName

  case exts of
    ".ktx" ->
      liftIO $ Ktx1.toFile outputName ktx1

    ".ktx.zst" -> do
      let
        Zstd.Compressed bytes =
          ktx1
            & Ktx1.toBuilder
            & Builder.toLazyByteString
            & BSL.toStrict
            & Zstd.compressBytes
      BS.writeFile outputName bytes

    ".ktx2" ->
      let
        (header, dfd, kvd, levels) = imageToKtx2 tilemap
      in
        Ktx2Write.toFile
          outputName
          header
          dfd
          kvd
          mempty -- no SGD
          levels

    ".csv" -> do
      tilesetWidth <- case tilesetWidthOpt of
        Nothing -> do
          logError "CSV export requires "
          exitFailure
        Just width ->
          pure width

      let
        pixels =
          Storable.unsafeCast @Word8 @tile imageData
      let
        tilesTable = Vector.unfoldr @Vector
          (\rest ->
              if Vector.null rest then
                Nothing
              else
                let
                  (row, next) = Vector.splitAt imageWidth rest
                in
                  Just
                    ( map
                        (toGlobalId FIRST_GID tilesetWidth)
                        (Vector.toList row)
                    , next
                    )
          )
          pixels
      let
        opts = CSV.EncodeOptions
          { encDelimiter     = 44 -- XXX: comma
          , encUseCrLf       = False
          , encIncludeHeader = False
          , encQuoting       = CSV.QuoteNone
          }

        byteStream = CSV.encodeWith opts $
          map (map Tiled.getGID) $ Vector.toList tilesTable

      liftIO $
        BSL.writeFile outputName byteStream

    ".tmj" ->
      encodeTiled
        @tile
        @pixel
        tilesetWidthOpt
        tilesetHeightOpt
        tilesetImageOpt
        outputName
        tilemap

    _unknown -> do
      logError $ "Unknown export extension: " <> displayShow exts
      exitFailure

  where
    (_name, exts) = splitExtensions outputName

    Picture.Image{..} = tilemap

    ktx1 = imageToKtx1 tilemap

imageToKtx2
  :: forall pixel
  . ( ToVkFormat pixel
    , Picture.PixelBaseComponent pixel ~ Word8
    )
  => Picture.Image pixel
  -> (Ktx2.Header, Vector DFD.Block, KtxKV.KeyValueData, [(Maybe Word64, ByteString)])
imageToKtx2 Picture.Image{..} =
  ( Ktx2.prepare
      (fromIntegral format)
      typeSize
      imageWidth
      imageHeight
      1
      Ktx2.SC_ZSTANDARD
  , [DFD.toBlock DFD.unspecified]
  , kvd
  , [(Just $ fromIntegral len, bytesZ)]
  )
  where
    kvd =
      [ (KtxKV.KTXorientation, KtxKV.text "rd")
      , (KtxKV.KTXwriter, KtxKV.text "keid-weld / ktx-codec")
      ]
    (bytesPtr, offset, len) = Storable.unsafeToForeignPtr imageData
    bytes = BSI.fromForeignPtr bytesPtr offset len
    Zstd.Compressed bytesZ = Zstd.compressBytes bytes

    (Vk.Format format, typeSize) = toVkFormat @pixel

csvTilesetWidth :: HasLogFunc env => Maybe a -> RIO env a
csvTilesetWidth = \case
  Nothing -> do
    logError "Transcoding CSV files requires --tileset-width"
    exitFailure
  Just tilesetWidth ->
    pure tilesetWidth

imageToKtx1
  :: forall pixel
  . ( ToGlFormat pixel
    , Picture.PixelBaseComponent pixel ~ Word8
    )
  => Picture.Image pixel
  -> Ktx1.Ktx
imageToKtx1 Picture.Image{..} = Ktx1.Ktx{..}
  where
    (bytesPtr, offset, len) = Storable.unsafeToForeignPtr imageData
    bytes = BSI.fromForeignPtr bytesPtr offset len

    (pixelComponent, pixelComponentSize, pixelFormat, pixelInternalFormat) =
      toGlFormat @pixel

    header = Ktx1.Header
      { identifier            = Ktx1.canonicalIdentifier
      , endianness            = Ktx1.endiannessLE
      , glType                = pixelComponent
      , glTypeSize            = pixelComponentSize
      , glFormat              = pixelFormat
      , glInternalFormat      = pixelInternalFormat
      , glBaseInternalFormat  = pixelInternalFormat
      , pixelWidth            = fromIntegral imageWidth
      , pixelHeight           = fromIntegral imageHeight
      , pixelDepth            = 0
      , numberOfArrayElements = 0
      , numberOfFaces         = 1
      , numberOfMipmapLevels  = 1
      , bytesOfKeyValueData   = kvsBytes
      }

    kvsBytes = 28
    kvs = Map.fromList
      [ ( "KTXorientation"
        , KtxKV.text "S=r,T=d"
        )
      ]

    images =
      [ Ktx1.MipLevel
          { imageSize =
              fromIntegral len
          , arrayElements =
              [ Ktx1.ArrayElement
                  { faces =
                      [ Ktx1.Face
                          { zSlices =
                              [ Ktx1.ZSlice bytes
                              ]
                          }
                      ]
                  }
              ]
          }
      ]
